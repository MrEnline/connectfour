﻿using System;

namespace ConnectFour
{
    class Program
    {
        Game game;
        Computer comp;

        static void Main(string[] args)
        {
            Program program = new Program();
            program.StartGame();

            Console.ReadKey();
        }

        void StartGame()
        {
            game = new Game();
            comp = new Computer(game.GetMap());
            while (true)
            {
                if (game.IsPlayable())
                {
                    Print();
                    int column = Convert.ToInt32(Console.ReadLine());
                    game.MakeMove(column);
                    column = comp.FindMove(2);
                    game.MakeMove(column);
                }
                else
                {
                    Print();
                    Console.WriteLine(game.GetGameStatus().ToString());
                    Console.WriteLine("Хотите начать занаво игру?");
                    if (Console.ReadLine().ToLower() == "yes" || Console.ReadLine().ToLower() == "да"  && 
                        Console.ReadLine().ToLower() != "exit")
                        game.Restart();
                    else 
                        break;
                }
            }
        }

        void Print()
        {
            for (int y = 5; y >= 0; y--)
            {
                for (int x = 0; x < 7; x++)
                    Console.Write(game.GetSymbolAt(x, y) + " ");
                Console.WriteLine();
            }
            Console.WriteLine("0 1 2 3 4 5 6");
        }
    }
}
