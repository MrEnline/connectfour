﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConnectFour
{
    class Computer
    {
        int[,] map;
        int p1, p2;
        List<string> listPattern;

        public Computer(int[,] map)
        {
            this.map = map;
            listPattern = new List<string>();
            InitListPattern();
        }

        private void InitListPattern()
        {
            listPattern.Add(".xxx");
            listPattern.Add("xxx.");
            listPattern.Add("x.xx");
            listPattern.Add("xx.x");
            
            listPattern.Add("ooo.");
            listPattern.Add(".ooo");
            listPattern.Add("o.oo");
            listPattern.Add("oo.o");

            listPattern.Add(".oo.");
            listPattern.Add("oo..");
            listPattern.Add("o.o.");
            listPattern.Add("o..o");

            listPattern.Add(".oo");
            listPattern.Add("oo.");
            listPattern.Add("o.o");
            listPattern.Add("o.o");

            listPattern.Add(".xx.");
            listPattern.Add("xx..");
            listPattern.Add("x.x.");
            listPattern.Add("x..x");

            listPattern.Add(".xx");
            listPattern.Add("xx.");
            listPattern.Add("x.x");
            listPattern.Add("x.x");
        }

        public int FindMove(int coin)
        {
            int tmpColumn = 0;
            for (int column = 0; column < 7; column++)
            {
                tmpColumn = column;
                foreach (var pattern in listPattern)
                {
                    if (FindPattern(pattern))
                        return p1;
                }
            }
            return tmpColumn;
        }

        bool FindPattern(string pattern)
        {
            for (int x = 0; x < 7; x++)
                for (int y = 0; y < 6; y++)
                {
                    if (IsPattern(x, y, 1, 0, pattern)) return true;
                    if (IsPattern(x, y, 1, 1, pattern)) return true;
                    if (IsPattern(x, y, 0, 1, pattern)) return true;
                    if (IsPattern(x, y, -1, -1, pattern)) return true;
                }
            return false;
        }

        bool IsPattern(int x0, int y0, int sx, int sy, string pattern)
        {
            p1 = 0; p2 = 0;
            int x = 0, y = 0;
            for (int i = 0; i < pattern.Length; i++)
            {
                x = x0 + i * sx;
                y = y0 + i * sy;
                if (!OnMap(x, y))
                    return false;
                if (!Compare(map[x, y], pattern[i]))
                    return false;
                if (Compare(map[x, y], pattern[i]) && pattern[i] == '.')
                    p1 = x;
            }
            if (p1 == 0)
                p1 = x;
            Console.WriteLine($"X = {p1}");
            return true;
        }

        bool Compare(int coin, char symbol)
        {
            switch (coin)
            {
                case 0: return symbol == '.';
                case 1: return symbol == 'x';
                case 2: return symbol == 'o';
                default:
                    return false;
            }
        }

        //ограничения для поля игры
        private bool OnMap(int x, int y)
        {
            return x >= 0 && x < 7 && y >= 0 && y < 6;
        }

        //бросили монетку
        private bool DownCoin(int coin, int column)
        {
            for (int i = 0; i < 6; i++)
            {
                if (map[column, i] == 0)
                {
                    map[column, i] = coin;
                    return true;
                }
            }
            return false;
        }

        //вытащили монетку - откат к исходному значению данной ячейки
        private void UpCoin(int column)
        {
            for (int i = 5; i >= 0; i--)
            {
                if (map[column, i] != 0)
                {
                    map[column, i] = 0;
                    return;
                }
            }
        }
    }
}
