﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConnectFour
{
    enum GameStatus
    {
        whiteMove,
        blackMove,
        whiteWin,
        blackWin,
        draw
    }

    class Game
    {
        int[,] map;                 // 0 - empty, 1 - white, 2 - black
        GameStatus gameStatus;
        int moves;                  //количество шагов

        public Game() 
        {
            Restart();
        }

        public void Restart()
        {
            map = new int[7, 6];
            gameStatus = GameStatus.whiteMove;
        }

        public void MakeMove(int column)
        {
            switch (gameStatus)
            {
                case GameStatus.whiteMove:
                    if (!DownCoin(1, column))
                        return;
                    gameStatus = GameStatus.blackMove;
                    if (Win(1))
                        gameStatus = GameStatus.whiteWin;
                    break;
                case GameStatus.blackMove:
                    if (!DownCoin(2, column))
                        return;
                    gameStatus = GameStatus.whiteMove;
                    if (Win(2))
                        gameStatus = GameStatus.blackWin;
                    break;
                default:
                    break;
            }
        }

        private bool DownCoin(int coin, int column)
        {
            for (int i = 0; i < 6; i++)
            {
                if (map[column, i] == 0)
                {
                    map[column, i] = coin;
                    moves++;
                    return true;
                }
            }
            return false;
        }

        private bool Win(int coin)
        {
            for (int x = 0; x < 7; x++)
                for (int y = 0; y < 6; y++)
                {
                    if (IsFourCoins(x, y, 1, 0, coin)) return true;
                    if (IsFourCoins(x, y, 1, 1, coin)) return true;
                    if (IsFourCoins(x, y, 0, 1, coin)) return true;
                    if (IsFourCoins(x, y, -1, -1, coin)) return true;
                }
            return false;
        }

        bool IsFourCoins(int x0, int y0, int sx, int sy, int coin)
        {
            for (int i = 0; i < 4; i++)
            {
                int x = x0 + i * sx;
                int y = y0 + i * sy;
                if (!OnMap(x, y))
                    return false;
                if (map[x, y] != coin)
                    return false;
            }
            return true;
        }

        //проверка границ
        private bool OnMap(int x, int y)
        {
            return x >= 0 && x < 7 && y >= 0 && y < 6;
        }

        public GameStatus GetGameStatus()
        {
            return gameStatus;
        }

        public char GetSymbolAt(int x, int y) 
        {
            switch (map[x, y])
            {
                case 0: return '.';
                case 1: return 'x';
                case 2: return 'o';
                default: return ' ';
            }
        }

        public bool IsPlayable()
        {
            return gameStatus == GameStatus.blackMove || gameStatus == GameStatus.whiteMove;
        }

        public int[,] GetMap() => (int[,])map;

    }
}
